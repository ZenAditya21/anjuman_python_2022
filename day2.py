# list of dict
from prettytable import PrettyTable

emps = [
    {'name':"priya", 'salary': 34000}, 
    {'name':"juhi", 'salary': 30000}, 
    {'name':"kunal", 'salary': 24000}, 
    {'name':"shoyab", 'salary': 54000}, 
    {'name':"lokesh", 'salary': 64000}
]

myTable = PrettyTable(['name', 'salary'])
# print(myTable)

for rec in emps:
    # print(rec['name'])
    # print(rec['salary'])
    myTable.add_row([rec['name'], rec['salary']])

print(myTable)

total = 0
max = emps[0]['salary']
min = emps[0]['salary']

for x in emps:
    if (x['salary'] > max):
        max = x['salary']

    if (x['salary'] < min):
        min = x['salary']

    total = total + x['salary']

print(f'max salary: {max}')
print(f'min salary: {min}')
print(f'total salary: {total}')
print(f'average salary: {total / 5}')



'''
print(emps[0]['salary'])
print(emps[3])
print(emps[4]['name'])
'''





















# nums = [1, 2, 3,4, 5]
# std = ["priya", 18, "nagpur"]
'''
std1 = {
    'name' : "priya", 
    'age' : 18, 
    'city' : "nagpur"
}
std2 = {
    'name' : "juhi", 
    'age' : 18, 
    'city' : "panjra"
}
print(std1['age'])
# print(std[2])
'''
